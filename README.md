环境：
ubuntu 20.04LTS 
node.js 14.x.xLTS
Postgresql
redis

使用figma做prototype：https://www.figma.com/file/xlNSbkRmdZedLrhwRrcHv7/Untitled?node-id=0%3A1

1. 目录设置，将前端代码放在chatAppFront目录下，将后端代码放在chatAppBack目录下。注意这是一个前后端分离的web app，因此前端后端需要分别启动。
2. node.js 环境配置，在网络上找到nodejs的安装教程，开发的时候使用的是14.x.x版本，可以选择大版本一致的node js。
3. 前端环境配置，这里使用的是yarn来管理项目。先进入chatAppFront目录下，运行yarn install 下载一些用到的组件。下载完成之后，可以运行yarn start,即可运行前端，可能会碰到react-script not found 或者permission denied 的问题，需要下载react-script，建议google一下，如何解决，stack overflow有解答。
4. 后端环境配置，后端环境稍微复杂一些，先配置postgresql,然后是knex以及redis，下面一个一个讲:
5. postgresql 配置：
 * sudo apt install postgresql 下载postgresql
 * sudo service postgresql start 开启postgresql 服务
 * 跟着该链接，修改postgres用户的密码，包括linux系统和数据库内部的用户https://www.cnblogs.com/kaituorensheng/p/4735191.html
 * 使用psql创建一个新的数据库 
    * sudo -u postgres psql 该命令会打开postgresql命令行工具
    * 在psql中输入：CREATE DATABASE cat_chup; (不要忘记分号)

6. redis 配置：
 * sudo apt install redis 
 * service redis start (之类的启动服务的命令) https://phoenixnap.com/kb/install-redis-on-ubuntu-20-04

7. knex配置:
 * 在chatAppBack（后端目录）下运行 npm install --global knex nodemon
 * 下载完成之后，运行knex migrate:latest (这一步是运行数据库表格创建等操作)

8. 然后nodemon app.js 即可运行后端。